/**
 * Created by WVega1697 on 05/05/2016.
 */
(function() {
    var express = require('express');
    var bodyParser = require('body-parser');
    var morgan = require('morgan');
    var mysql= require('mysql');
    var Sequelize= require('sequelize');

    var sequelize = new Sequelize('db_turismo2011141', 'root', '',{
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 20,
            min: 0
        }
    });

    /*
        Declaraciones de los modelos
     */
    var Usuario = sequelize.define('usuario', {
        id_usuario: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        nombre: { type: Sequelize.STRING, allowNull: false},
        correo: { type: Sequelize.STRING, allowNull: false},
        contra: { type: Sequelize.STRING, allowNull: false},
    });

    var Departamento = sequelize.define('departamento', {
        id_departamento: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        nombre: { type: Sequelize.STRING, allowNull: false},
        descripcion: { type: Sequelize.STRING, allowNull: false},
    });

    var LugarTuristico = sequelize.define('lugarturistico', {
        id_lugarturistico: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        nombre: { type: Sequelize.STRING, allowNull: false},
        descripcion: { type: Sequelize.STRING, allowNull: false},
        costo: {type: Sequelize.DOUBLE, allowNull: false},
        id_departamento: { type: Sequelize.INTEGER, foreignKey: true}
    });

    var Hotel = sequelize.define('hotel', {
        id_hotel: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        nombre: { type: Sequelize.STRING, allowNull: false},
        descripcion: { type: Sequelize.STRING, allowNull: false},
        puntuacion: { type: Sequelize.INTEGER, allowNull: false},
        costo: {type: Sequelize.DOUBLE, allowNull: false}
    });

    /*var TipoArchivo = sequelize.define('tipoarchivo', {
        id_tipoarchivo: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        nombre: { type: Sequelize.STRING, allowNull: false}
    });

    var Archivo = sequelize.define('archivo', {
        id_archivo: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        id_lugarturistico: { type: Sequelize.INTEGER, foreignKey: true},
        id_tipoarchivo: { type: Sequelize.INTEGER, foreignKey: true}
    });*/

    var Comentario = sequelize.define('comentario', {
        id_comentario: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        contenido: { type: Sequelize.STRING, allowNull: false}
    });

    var Reporte = sequelize.define('comentario', {
        id_comentario: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        contenido: { type: Sequelize.STRING, allowNull: false}
    });

    //Lugar Turistico
    Departamento.hasMany(LugarTuristico, { foreignKey: 'id_departamento', constraints: true});
    LugarTuristico.belongsTo(Departamento, { foreignKey: 'id_departamento',  constraints: true});
    //Hotel
    Departamento.hasMany(Hotel, { constraints: true});
    Hotel.belongsTo(Departamento, { constraints: true});
    //Comentario
    Hotel.hasMany(Comentario, { constraints: true});
    Comentario.belongsTo(Hotel, { constraints: true});
    LugarTuristico.hasMany(Comentario, { constraints: true});
    Comentario.belongsTo(LugarTuristico, { constraints: true});

    sequelize.sync({ force: true});
    var puerto = 3000;
    var conf=require('./config');
    var app=express();
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use('/api/v1', require('./routes')(app));

    app.use(morgan('dev'))
    app.set('departamento', Departamento);
    app.set('lugarturistico', LugarTuristico);
    app.listen(puerto,function () {
        console.log("Servidor iniciado en el puerto: " + puerto);
        console.log("Debug del server:");
    });
})();